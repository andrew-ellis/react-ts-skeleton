import React from 'react';
import { render } from 'react-dom';
import NavBar from './NavBar';

export interface State {
  [key: string]: any;
}

class App extends React.Component<{}, State> {
  public state = {};

  public render() {
    return (
      <div>
        <NavBar />
      </div>
    );
  }
}

render(<App />, document.getElementById('root'));
