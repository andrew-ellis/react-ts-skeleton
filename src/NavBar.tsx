import styled from 'react-emotion';
import React from 'react';
import { Link } from '@reach/router';
import colors from './colors';

const Container = styled('header')`
  height: 50px;
  background-color: ${colors.dark};
  position: sticky;
  top: 0;
  z-index: 10;
`;

const NavLink = styled(Link)`
  color: #ddd;
  font-size: 18px;
  line-height: 50px;
  vertical-align: middle;
  &:hover {
    text-decoration: underline;
  }
`;

const NavBar = () => (
  <Container>
    <NavLink to="/">Home</NavLink>
  </Container>
);

export default NavBar;
